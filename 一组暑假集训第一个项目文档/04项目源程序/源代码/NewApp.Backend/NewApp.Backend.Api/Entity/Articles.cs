namespace NewApp.Backend.Api.Entity
{
    public class Articles : BaseEntity
    {
        //创建人Id
        public int FromUsersId { get; set; }
        //文章标题
        public string ArticlesTitle { get; set; }
        //文章简介
        public string ArticleSynopsis { get; set; }
        //文章内容
        public string ArticlesContent { get; set; }
        //文章分类
        public string ArticlesClassify { get; set; }
        //文章封面图片路径
        public string ArticleCovers { get; set; }
    }
}