namespace NewApp.Backend.Api.Entity
{
    public class ArticleLikes:BaseEntity
    {
        //点赞人的Id
        public int FromUsersId {get;set;}
        //被点赞的文章Id
        public int FromArticlesId {get;set;}
    }
}