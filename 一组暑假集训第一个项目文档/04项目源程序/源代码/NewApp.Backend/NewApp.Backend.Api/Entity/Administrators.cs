namespace NewApp.Backend.Api.Entity
{
    public class Administrators:BaseEntity
    {
        //管理员用户名
        public string AdministratorsUsername {get;set;}
        //管理员密码
        public string AdministratorsPassword {get;set;}
    }
}