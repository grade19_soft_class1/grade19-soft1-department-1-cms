namespace NewApp.Backend.Api.Entity
{
    //公司动态
    public class CompanyDynamics : BaseEntity
    {
        //事件主题
        public string CompanTitle { get; set; }
        //公司事件内容  （公司动态）
        public string CompanyEvent { get; set; }
    }
}