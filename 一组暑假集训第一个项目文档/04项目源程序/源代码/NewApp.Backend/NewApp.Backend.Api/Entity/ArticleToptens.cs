namespace NewApp.Backend.Api.Entity
{
    //文章排行
    public class ArticleToptens:BaseEntity
    {
        //文章Id
        public int FromArticlesId {get;set;}
        //点赞数量
        public int LikeSum {get;set;}
        //阅读数量
        public int ReadSum {get;set;}
        //评论数量
        public int CommentSum {get;set;}
        
    }
}