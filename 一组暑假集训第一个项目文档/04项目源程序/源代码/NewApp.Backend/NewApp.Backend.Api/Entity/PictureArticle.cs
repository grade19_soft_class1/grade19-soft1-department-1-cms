namespace NewApp.Backend.Api.Entity
{
    public class PictureArticle : BaseEntity
    {   
        //图片路径
        public string Url { get; set; }
        //文字提示
        public string Alt { get; set; }
        //跳转链接
        public string Href { get; set; }
    }
}