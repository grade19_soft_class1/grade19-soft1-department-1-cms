namespace NewApp.Backend.Api.Entity
{
    public class ArticleReader:BaseEntity
    {
        //阅读人Id
        public int FromUsersId {get;set;}
        //所阅读文章Id
        public int FromArticlesId {get;set;}
    }
}