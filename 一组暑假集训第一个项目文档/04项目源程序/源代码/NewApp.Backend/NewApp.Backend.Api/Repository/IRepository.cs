using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using NewApp.Backend.Api.Entity;

namespace NewApp.Backend.Api.Repository
{
    /// <summary>
    /// 定义一个接口，用于实体类型的CRUD操作
    /// </summary>
    /// <typeparam name="T">泛型，可以是任意一个实体类型</typeparam>
    public interface IRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// 一个属性，表示可查询的表
        /// </summary>
        /// <value></value>
        IQueryable<T> Table { get; }

        /// <summary>
        /// 根据提供的id，查询对应的T类型实例
        /// </summary>
        /// <param name="id">提供的id，这里可能是int、long、guid等常见的主键类型</param>
        /// <returns></returns>
        T GetById(object id);

        /// <summary>
        /// 根据提供的实体对象，插入到数据库
        /// </summary>
        /// <param name="entity">提供的实体对象</param>
        void Insert(T entity);

        /// <summary>
        /// 根据提供的实体对象，插入到数据库（异步）
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task InsertAsync(T entity);

        /// <summary>
        /// 根据提供的实体对象集合，批量插入到数据库
        /// </summary>
        /// <param name="entities"></param>
        void InsertBulk(IEnumerable<T> entities);

        /// <summary>
        /// 根据提供的实体对象集合，批量插入到数据库（异步）
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        Task InsertBulkAsync(IEnumerable<T> entities);

        /// <summary>
        /// 根据提供的主键id，删除数据库中对应的记录
        /// </summary>
        /// <param name="id">提供的id，这里可能是int、long、guid等常见的主键类型</param>
        void Delete(object id);

        /// <summary>
        /// 根据提供的主键id集合，批量删除数据库中对应的记录
        /// </summary>
        /// <param name="ids">提供的id的集合</param>
        void DeleteBulk(IEnumerable<object> ids);

        /// <summary>
        /// 根据提供的实体对象，更新实体到数据库
        /// </summary>
        /// <param name="entity">实体对象</param>
        void Update(T entity);

        /// <summary>
        /// 根据提供的实体对象集合，批量更新实体到数据库
        /// </summary>
        /// <param name="entities">实体对象集合</param>
        void UpdateBulk(IEnumerable<T> entities);
    }
}