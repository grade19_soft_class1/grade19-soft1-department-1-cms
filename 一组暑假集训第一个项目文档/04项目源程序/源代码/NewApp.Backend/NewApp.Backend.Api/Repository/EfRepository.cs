using NewApp.Backend.Api.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NewApp.Backend.Api.Database;
using Microsoft.EntityFrameworkCore;
using System;

namespace NewApp.Backend.Api.Repository
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private NewAppDb _db;

        public EfRepository(NewAppDb db)
        {
            _db = db;
        }

        private DbSet<T> _entity;

        protected DbSet<T> Entity
        {
            get
            {
                if (_entity == null)
                {
                    _entity = _db.Set<T>();
                }
                return _entity;
            }
        }

        public IQueryable<T> Table
        {
            get
            {
                return Entity.AsQueryable<T>();
            }
        }

        public void Delete(object id)
        {
            var t = Entity.Find(id);
            if (t == null)
            {
                throw new ArgumentNullException(nameof(t));
            }
            _db.Remove(t);
            _db.SaveChanges();
        }

        public void DeleteBulk(IEnumerable<object> ids)
        {
            var ii = new List<int>();
            foreach (var item in ids)
            {
                var tmp = (int)item;
                ii.Add(tmp);
            }
            var ts = Entity.Where(x => ii.Contains(x.Id)).ToList();
            _db.RemoveRange(ts);
            _db.SaveChanges();
        }

        public T GetById(object id)
        {
            return Entity.Find(id);
        }

        public void Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            entity.IsActived = true;
            entity.IsDeleted = false;
            entity.CreatedTime = DateTime.Now;
            entity.UpdatedTime = DateTime.Now;
            entity.DisplayOrder = 0;

            Entity.Add(entity);
            _db.SaveChanges();
        }

        public async Task InsertAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            entity.IsActived = true;
            entity.IsDeleted = false;
            entity.CreatedTime = DateTime.Now;
            entity.UpdatedTime = DateTime.Now;
            entity.DisplayOrder = 0;

            await Entity.AddAsync(entity);
            await _db.SaveChangesAsync();
        }

        public void InsertBulk(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.IsActived = true;
                entity.IsDeleted = false;
                entity.CreatedTime = DateTime.Now;
                entity.UpdatedTime = DateTime.Now;
                entity.DisplayOrder = 0;
            }
            Entity.AddRange(entities);
            _db.SaveChanges();
        }

        public async Task InsertBulkAsync(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.IsActived = true;
                entity.IsDeleted = false;
                entity.CreatedTime = DateTime.Now;
                entity.UpdatedTime = DateTime.Now;
                entity.DisplayOrder = 0;
            }
            await Entity.AddRangeAsync(entities);
            await _db.SaveChangesAsync();
        }

        public void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            entity.UpdatedTime = DateTime.Now;
            _db.SaveChanges();
        }

        public void UpdateBulk(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.UpdatedTime = DateTime.Now;
            }

            Entity.UpdateRange(entities);
            _db.SaveChanges();
        }
    }
}