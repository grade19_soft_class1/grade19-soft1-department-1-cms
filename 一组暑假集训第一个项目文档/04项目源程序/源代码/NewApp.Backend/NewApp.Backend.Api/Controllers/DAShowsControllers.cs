using System.Linq;
using NewApp.Backend.Api.Entity;
using NewApp.Backend.Api.Params;
using NewApp.Backend.Api.Repository;
using NewApp.Backend.Api.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace NewApp.Backend.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]

    //这里的控制器是 DAShows (大神风采)
    public class DAShowsController : ControllerBase
    {

        private IConfiguration _configuration;

        private IRepository<DAShows> _DAShowsRepository;

        private TokenParameter _tokenParameter;

        public DAShowsController(IConfiguration configuration
        , IRepository<DAShows> DAShowsRepository)
        {
            _configuration = configuration;
            _DAShowsRepository = DAShowsRepository;
            _tokenParameter =
                _configuration
                    .GetSection("TokenParameter")
                    .Get<TokenParameter>();
        }

        //获取所有数据库中的大神风采
        [HttpGet]
        public dynamic Get()//[FromQuery] Pager pager)
        {
            // get请求默认从url中获取参数，如果需要使用实体接收参数，需要FromQuery特性
            // var pageIndex = pager.pageIndex;
            // var pageSize = pager.pageSize;
            // var users = _usersRepository.Table;
            // var u = users.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            var dAShows = _DAShowsRepository.Table.ToList();

            return new
            {
                Code = 1000,
                Data = new
                {
                    Data = dAShows,
                    // Pager = new
                    // {
                    //     pageIndex,
                    //     pageSize,
                    //     rowsTotal = users.Count()
                    // }
                    // rowsTotal = newList.Count()
                },
                Msg = "获取大神风采成功^_^"
            };
        }

        //添加
        [HttpPost]
        public dynamic Post(CreteDAShow creteDAShow)
        {

            var daTitle = creteDAShow.DATitle.Trim();
            var daEvent = creteDAShow.DAEvent.Trim();

            if (string.IsNullOrEmpty(daTitle) && string.IsNullOrEmpty(daEvent))
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "大神风采的标题或内容不能为空"
                };

            }

            var shows = new DAShows
            {
                DATitle = creteDAShow.DATitle,
                DAEvent = creteDAShow.DAEvent,
                // Remarks = string.IsNullOrEmpty(creteDAShow.Remarks.Trim()) ? "无" : creteDAShow.Remarks
            };

            _DAShowsRepository.Insert(shows);

            return new
            {
                Code = 1000,
                Data = shows,
                Msg = "创建大神风采成功^_^"
            };

        }

        //修改
        [HttpPut("{id}")]
        public dynamic Put(int id, CreteDAShow updateDAShow)
        {

            var daTitle = updateDAShow.DATitle.Trim();
            var daEvent = updateDAShow.DAEvent.Trim();

            if (string.IsNullOrEmpty(daTitle) && string.IsNullOrEmpty(daEvent))
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "大神风采或内容不能为空"
                };

            }
            var daShows = _DAShowsRepository.GetById(id);

            if (daShows == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "要更新的大神风采不存在，请确认后重试=_="
                };
            }

            daShows.DATitle = updateDAShow.DATitle;
            daShows.DAEvent= updateDAShow.DAEvent;
            // daShows.Remarks = string.IsNullOrEmpty(updateDAShow.Remarks.Trim()) ? "无" : updateDAShow.Remarks;

            _DAShowsRepository.Update(daShows);

            return new
            {
                Code = 1000,
                Data = daShows,
                Msg = "更新成功"
            };

        }

        //删除
        [HttpDelete("{id}")]
        public dynamic Delete(int id)
        {
            _DAShowsRepository.Delete(id);
            
            return new
            {
                Code = 1000,
                Data = "",
                Msg = "删除大神风采成功^_^"
            };
        }
    }
}