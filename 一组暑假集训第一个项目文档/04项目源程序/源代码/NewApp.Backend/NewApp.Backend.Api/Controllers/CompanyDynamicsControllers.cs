using System.Linq;
using NewApp.Backend.Api.Entity;
using NewApp.Backend.Api.Params;
using NewApp.Backend.Api.Repository;
using NewApp.Backend.Api.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace NewApp.Backend.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]

    //这里的控制器是 CompanyDynamics (公司动态)
    public class CompanyDynamicsController : ControllerBase
    {

        private IConfiguration _configuration;

        private IRepository<CompanyDynamics> _CompanyDynamicsRepository;

        private TokenParameter _tokenParameter;

        public CompanyDynamicsController(IConfiguration configuration
        , IRepository<CompanyDynamics> CompanyDynamicsRepository)
        {
            _configuration = configuration;
            _CompanyDynamicsRepository = CompanyDynamicsRepository;
            _tokenParameter =
                _configuration
                    .GetSection("TokenParameter")
                    .Get<TokenParameter>();
        }

        //获取所有数据库中的公司动态
        [HttpGet]
        public dynamic Get()//[FromQuery] Pager pager)
        {
            // get请求默认从url中获取参数，如果需要使用实体接收参数，需要FromQuery特性
            // var pageIndex = pager.pageIndex;
            // var pageSize = pager.pageSize;
            // var users = _usersRepository.Table;
            // var u = users.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            var CompanyDynamic = _CompanyDynamicsRepository.Table.ToList();

            return new
            {
                Code = 1000,
                Data = new
                {
                    Data = CompanyDynamic,
                    // Pager = new
                    // {
                    //     pageIndex,
                    //     pageSize,
                    //     rowsTotal = users.Count()
                    // }
                    // rowsTotal = newList.Count()
                },
                Msg = "获取公司动态列表成功^_^"
            };
        }

        //添加
        [HttpPost]
        public dynamic Post(CreateDynamics createDynamics)
        {

            var companTitle = createDynamics.CompanTitle.Trim();
            var companyEvent = createDynamics.CompanyEvent.Trim();

            if (string.IsNullOrEmpty(companTitle) && string.IsNullOrEmpty(companyEvent))
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "公司动态的标题或内容不能为空"
                };

            }

            var news = new CompanyDynamics
            {
                CompanTitle = createDynamics.CompanTitle,
                CompanyEvent = createDynamics.CompanyEvent,
                // Remarks = string.IsNullOrEmpty(createDynamics.Remarks.Trim()) ? "无" : createDynamics.Remarks
            };

            _CompanyDynamicsRepository.Insert(news);

            return new
            {
                Code = 1000,
                Data = news,
                Msg = "创建公司动态成功^_^"
            };

        }

        //修改
        [HttpPut("{id}")]
        public dynamic Put(int id, CreateDynamics updateDynamics)
        {

            var companTitle = updateDynamics.CompanTitle.Trim();
            var companyEvent = updateDynamics.CompanyEvent.Trim();

            if (string.IsNullOrEmpty(companTitle) && string.IsNullOrEmpty(companyEvent))
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "新闻标题或内容不能为空"
                };

            }
            var companyDynamics = _CompanyDynamicsRepository.GetById(id);

            if (companyDynamics == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "要更新的新闻时事不存在，请确认后重试=_="
                };
            }

            companyDynamics.CompanTitle = updateDynamics.CompanTitle;
            companyDynamics.CompanyEvent= updateDynamics.CompanyEvent;
            // companyDynamics.Remarks = string.IsNullOrEmpty(updateDynamics.Remarks.Trim()) ? "无" : updateDynamics.Remarks;

            _CompanyDynamicsRepository.Update(companyDynamics);

            return new
            {
                Code = 1000,
                Data = companyDynamics,
                Msg = "更新成功"
            };

        }

        //删除
        [HttpDelete("{id}")]
        public dynamic Delete(int id)
        {
            _CompanyDynamicsRepository.Delete(id);
            
            return new
            {
                Code = 1000,
                Data = "",
                Msg = "删除新闻时事成功^_^"
            };
        }
    }
}