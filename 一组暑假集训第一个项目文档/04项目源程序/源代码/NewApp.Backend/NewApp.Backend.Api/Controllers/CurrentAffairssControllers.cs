using System.Linq;
using NewApp.Backend.Api.Entity;
using NewApp.Backend.Api.Params;
using NewApp.Backend.Api.Repository;
using NewApp.Backend.Api.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace NewApp.Backend.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]

    //这里的控制器是 CurrentAffairss 新闻时事
    public class CurrentAffairssController : ControllerBase
    {

        private IConfiguration _configuration;

        private IRepository<CurrentAffairss> _CurrentAddairssRepository;

        private TokenParameter _tokenParameter;

        public CurrentAffairssController(IConfiguration configuration
        , IRepository<CurrentAffairss> CurrentAddairssRepository)
        {
            _configuration = configuration;
            _CurrentAddairssRepository = CurrentAddairssRepository;
            _tokenParameter =
                _configuration
                    .GetSection("TokenParameter")
                    .Get<TokenParameter>();
        }

        //获取所有数据库中的列表
        [HttpGet]
        public dynamic Get()//[FromQuery] Pager pager)
        {
            // get请求默认从url中获取参数，如果需要使用实体接收参数，需要FromQuery特性
            // var pageIndex = pager.pageIndex;
            // var pageSize = pager.pageSize;
            // var users = _usersRepository.Table;
            // var u = users.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            var news = _CurrentAddairssRepository.Table.ToList();

            return new
            {
                Code = 1000,
                Data = new
                {
                    Data = news,
                    // Pager = new
                    // {
                    //     pageIndex,
                    //     pageSize,
                    //     rowsTotal = users.Count()
                    // }
                    // rowsTotal = newList.Count()
                },
                Msg = "获取新闻列表成功^_^"
            };
        }

        //添加
        [HttpPost]
        public dynamic Post(CreateNews createNews)
        {

            var newsTitle = createNews.NewsTitle.Trim();
            var newsEvent = createNews.NewsEvent.Trim();

            if (string.IsNullOrEmpty(newsTitle) && string.IsNullOrEmpty(newsEvent))
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "新闻标题或内容不能为空"
                };

            }

            var news = new CurrentAffairss
            {
                NewsTitle = createNews.NewsTitle,
                NewsEvent = createNews.NewsEvent,
                // Remarks = string.IsNullOrEmpty(createNews.Remarks.Trim()) ? "无" : createNews.Remarks
            };

            _CurrentAddairssRepository.Insert(news);

            return new
            {
                Code = 1000,
                Data = news,
                Msg = "创建新闻时事成功^_^"
            };

        }

        //修改
        [HttpPut("{id}")]
        public dynamic Put(int id, CreateNews updateNews)
        {

            var newsTitle = updateNews.NewsTitle.Trim();
            var newsEvent = updateNews.NewsEvent.Trim();

            if (string.IsNullOrEmpty(newsTitle) && string.IsNullOrEmpty(newsEvent))
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "新闻标题或内容不能为空"
                };

            }
            var news = _CurrentAddairssRepository.GetById(id);

            if (news == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "要更新的新闻时事不存在，请确认后重试=_="
                };
            }

            news.NewsTitle = updateNews.NewsTitle;
            news.NewsEvent = updateNews.NewsEvent;
            // news.Remarks = string.IsNullOrEmpty(updateNews.Remarks.Trim()) ? "无" : updateNews.Remarks;

            _CurrentAddairssRepository.Update(news);

            return new
            {
                Code = 1000,
                Data = news,
                Msg = "更新成功"
            };

        }
        [HttpDelete("{id}")]

        public dynamic Delete(int id)
        {
            _CurrentAddairssRepository.Delete(id);
            return new
            {
                Code = 1000,
                Data = "",
                Msg = "删除新闻时事成功^_^"
            };
        }
    }
}