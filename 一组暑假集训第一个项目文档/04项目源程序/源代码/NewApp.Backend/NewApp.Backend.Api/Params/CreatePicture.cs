namespace NewApp.Backend.Api.Params
{
    public class CreatePicture
    {
        //图片名字
        public string PictureName { get; set; }
        //图片相对路径
        public string PicturePath { get; set; }
        //图片状态
        public string status { get; set; }
    }
}