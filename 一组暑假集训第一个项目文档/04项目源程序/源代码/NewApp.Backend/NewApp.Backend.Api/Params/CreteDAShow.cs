namespace NewApp.Backend.Api.Params
{
    public class CreteDAShow
    {
        public string DATitle { get; set; }
        public string DAEvent { get; set; }
        public string Remarks { get; set; }
    }
}