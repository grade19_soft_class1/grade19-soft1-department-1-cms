namespace NewApp.Backend.Api.Params
{
    public class Pager
    {
        private int _pageIndex;
        private int _pageSize;
        public Pager()
        {
            _pageSize = 1;
            _pageSize = 10;
        }
        // 页码（当前第几页）
        public int PageIndex
        {
            get
            {
                if (_pageIndex < 1)
                {
                    _pageIndex = 1;
                }
                return _pageIndex;
            }
            set
            {
                _pageIndex = value;
            }
        }

        // 页大小（一页里面行的数量）
        public int PageSize
        {
            get
            {
                if (_pageSize < 10)
                {
                    _pageSize = 10;
                }
                return _pageSize;
            }
            set
            {
                _pageSize = value;
            }
        }

        // 总记录数量
        public int RowsTotal { get; set; }
    }
}