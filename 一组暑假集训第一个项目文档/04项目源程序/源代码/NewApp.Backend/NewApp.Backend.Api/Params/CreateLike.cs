namespace NewApp.Backend.Api.Params
{
    public class CreateLike
    {
        public int FromUsersId { get; set; }
        //被点赞的文章Id
        public int FromArticlesId { get; set; }
    }
}