namespace NewApp.Backend.Api.Params
{
    public class CreateUser
    {
        public int id {get;set;}
        public string Username{get;set;}
        public string Password{get;set;}
        public string avatar{get;set;}
    }
}