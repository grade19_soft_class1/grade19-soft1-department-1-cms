namespace NewApp.Backend.Api.Params
{
    public class CreateArticle
    {
        public int FromUsersId { get; set; }
        public string ArticlesTitle { get; set; }
        public string ArticleSynopsis { get; set; }
        public string ArticlesContent { get; set; }
        public string ArticlesClassify { get; set; }
        //图片路径
        public string avatar {get;set;}
        public string Remarks { get; set; }
        public int DisplayOrder { get; set; }
    } 
}