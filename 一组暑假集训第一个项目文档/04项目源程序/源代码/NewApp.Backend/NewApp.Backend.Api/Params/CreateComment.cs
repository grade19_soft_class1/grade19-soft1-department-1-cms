namespace NewApp.Backend.Api.Params
{
    public class CreateComment
    {
        //评论人的Id
        public int FromUsersId { get; set; }
        //被评论的文章Id
        public int FromArticlesId { get; set; }
        //评论内容
        public string ArticleCommentsContent { get; set; }
    }
}