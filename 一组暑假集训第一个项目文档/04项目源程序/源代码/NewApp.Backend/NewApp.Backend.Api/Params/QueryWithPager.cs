
namespace NewApp.Backend.Api.Params
{
    public class QueryWithPager
    {
        public QueryWithPager(){
            Pager=new Pager();
        }
        public string Keyword { get; set; }
        public Pager Pager { get; set; }
    }
}