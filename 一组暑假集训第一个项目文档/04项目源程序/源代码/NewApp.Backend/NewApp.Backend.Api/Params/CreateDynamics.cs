namespace NewApp.Backend.Api.Params
{
    public class CreateDynamics
    {
        public string CompanTitle { get; set; }
        public string CompanyEvent { get; set; }
        public string Remarks { get; set; }
    }
}