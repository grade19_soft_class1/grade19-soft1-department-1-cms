namespace NewApp.Backend.Api.Params
{
    public class CreateNews
    {
        public string NewsTitle { get; set; }
        public string NewsEvent { get; set; }
        public string Remarks { get; set; }
    }
}