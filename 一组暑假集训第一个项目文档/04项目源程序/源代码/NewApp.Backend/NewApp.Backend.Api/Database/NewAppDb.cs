
using Microsoft.EntityFrameworkCore;
using NewApp.Backend.Api.Entity;
using System;

namespace NewApp.Backend.Api.Database
{
    public class NewAppDb : DbContext
    {
        public NewAppDb(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Administrators> Administrators { get; set; }
        public DbSet<Articles> Articles { get; set; }
        public DbSet<ArticleComments> ArticleComments { get; set; }
        public DbSet<ArticleLikes> ArticleLikes { get; set; }
        public DbSet<ArticleReader> ArticleReader { get; set; }
        public DbSet<ArticleToptens> ArticleToptens { get; set; }
        public DbSet<CompanyDynamics> CompanyDynamics { get; set; }
        public DbSet<CurrentAffairss> CurrentAffairss { get; set; }
        public DbSet<DAShows> DAShows { get; set; }
        //轮播图表
        public DbSet<PictureInfo> PictureInfo { get; set; }
        public DbSet<Users> Users { get; set; }
        //文章内图片
        public DbSet<PictureArticle> PictureArticle { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(@"server=.;database=NewApp;uid=sa;pwd=123456;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // 初始化一个管理员用户 虽然Id是自动生成，但此处必须明确指定
            modelBuilder.Entity<Users>().HasData(
                new Users
                {
                    Id = 1,
                    Username = "a001",
                    Password = "113",
                    IsActived = true,
                    IsDeleted = false,
                    CreatedTime = DateTime.Now,
                    UpdatedTime = DateTime.Now,
                    UserPicture="http://localhost:8080/6.jpg",
                    DisplayOrder = 0,
                    Remarks = "种子数据"
                }
            );
            base.OnModelCreating(modelBuilder);

            // 初始化一个超级管理员用户 
            modelBuilder.Entity<Administrators>().HasData(
                new Administrators
                {
                    Id = 1,
                    AdministratorsUsername = "admin",
                    AdministratorsPassword = "113",
                    IsActived = true,
                    IsDeleted = false,
                    CreatedTime = DateTime.Now,
                    UpdatedTime = DateTime.Now,
                    DisplayOrder = 0,
                    Remarks = "原始数据"
                }
            );
            base.OnModelCreating(modelBuilder);

        }

    }


}