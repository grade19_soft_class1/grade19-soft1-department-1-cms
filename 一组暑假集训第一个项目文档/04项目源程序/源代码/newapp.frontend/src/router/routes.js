import Index from '../components/Index'
import Layout from '../Backend/Layout'

let routes = [
  //首页路由
  {
    path: '/',
    component: Index,
    meta: {
      title: '首页',
      icon: 'el-icon-goods',
      hidden: true
    },
    children: [
      {
        path: '/',
        meta: {
          title: '系统设置',
          icon: 'el-icon-goods',
          hidden: true
        },
        component: () => import('../components/Main')
      },
      {
        path: 'news',
        meta: {
          title: '新闻时事',
          icon: 'el-icon-goods',
          hidden: true
        },
        component: () => import('../components/News')
      },
      {
        path: 'company',
        meta: {
          title: '公司动态',
          icon: 'el-icon-goods',
          hidden: true
        },
        component: () => import('../components/Company')
      },
      {
        path: 'okami',
        meta: {
          title: '大神风采',
          icon: 'el-icon-goods',
          hidden: true
        },
        component: () => import('../components/Okami')
      },
      {
        path: 'about',
        meta: {
          title: '关于我们',
          icon: 'el-icon-goods',
          hidden: true
        },
        component: () => import('../components/About')
      },
      {
        path: 'article',
        meta: {
          title: '文章',
          icon: 'el-icon-goods',
          hidden: true
        },
        component: () => import('../components/article')
      },
      {
        path: 'aboutus',
        meta: {
          title: '文章',
          icon: 'el-icon-goods',
          hidden: true
        },
        component: () => import('../components/aboutus')
      },
      {
        path: 'fortiei',
        meta: {
          title: '48小时',
          icon: 'el-icon-goods',
          hidden: true
        },
        component: () => import('../components/fortiei')
      },
      {
        path: 'tendaycentent',
        meta: {
          title: '10天',
          icon: 'el-icon-goods',
          hidden: true
        },
        component: () => import('../components/tendaycentent')
      },
      {
        path: 'tendaybianji',
        meta: {
          title: '10天',
          icon: 'el-icon-goods',
          hidden: true
        },
        component: () => import('../components/tendaybianji')
      },
    ]
  },






  //登录路由
  {
    path: '/login',
    meta: {
      title: '登录',
      icon: 'el-icon-goods',
      hidden: true
    },
    component: () => import('../views/login')
  },
  //注册路由
  {
    path: '/register',
    component: () => import('../views/register'),
    meta: {
      title: '注册',
      icon: 'el-icon-goods',
      hidden: true
    }
  },
  //后台路由
  //仪表盘
  {
    path: '/echarts',
    component: Layout,
    meta: {
      title: '仪表盘',
      icon: 'el-icon-coin',
      hidden: true,
    },
    children: [
      {
        path: 'echarts',
        meta: {
          title: '仪表盘',
          icon: 'el-icon-coin',
        },
        component: () => import('../Backend/echarts'),
      },
    ],
  },
  //文章内容管理
  {
    path: '/Layout',
    component: Layout,
    redirect: { path: '/echarts/echarts' },
    name: 'Layout',
    meta: {
      title: '文章内容管理',
      icon: 'el-icon-goods',
    },
    children: [
      {
        path: 'content',
        name: 'content',
        meta: {
          title: '文章管理',
          icon: 'el-icon-warning-outline',
        },
        component: () => import('../Backend/content'),
      },
      {
        path: 'publish',
        name: 'publish',
        meta: {
          title: '发表文章',
          icon: 'el-icon-warning-outline',
        },
        component: () => import('../Backend/publish')
      }
    ],
  },

  //用户管理
  {
    path: '/system',
    component: Layout,
    name: 'system',
    meta: {
      title: '用户管理',
      icon: 'el-icon-warning-outline',
    },
    children: [
      {
        path: 'information',
        name: 'information',
        meta: {
          title: '用户信息管理',
          icon: 'el-icon-warning-outline',
        },
        component: () => import('../Backend/information')
      },
      {
        path: 'jurisdiction',
        name: 'inforvmation',
        meta: {
          title: '用户权限管理',
          icon: 'el-icon-warning-outline',
          hidden: true,
        },
        component: () => import('../Backend/jurisdiction')
      }
    ]
  },

  //推荐管理
  {
    path: '/recommend',
    component: Layout,
    name: 'recommend',
    meta: {
      title: '推荐管理',
      icon: 'el-icon-warning-outline',
    },
    children: [
      {
        path: 'read',
        name: 'read',
        meta: {
          title: '48小时阅读排行',
          icon: 'el-icon-warning-outline',
        },
        component: () => import('../Backend/read')
      },
      {
        path: 'edit',
        name: 'edit',
        meta: {
          title: '10天编辑推荐',
          icon: 'el-icon-warning-outline',
          hidden: true
        },
        component: () => import('../Backend/edit')
      },
      {
        path: 'comment',
        name: 'comment',
        meta: {
          title: '10天评论排行',
          icon: 'el-icon-warning-outline',
        },
        component: () => import('../Backend/comment')
      },
    ]
  },
  //网站信息管理
  {
    path: '/www',
    component: Layout,
    name: 'www',
    meta: {
      title: '栏目管理',
      icon: 'el-icon-warning-outline',
    },
    children: [
      {
        path: 'currenAaffairs',
        name: 'currenAaffairs',
        meta: {
          title: '新闻时事',
          icon: 'el-icon-warning-outline',
          hidden: true
        },
        component: () => import('../Backend/currenAaffairs')
      },
      {
        path: 'dynamic',
        name: 'dynamic',
        meta: {
          title: '公司动态',
          icon: 'el-icon-warning-outline',
          hidden: true
        },
        component: () => import('../Backend/dynamic')
      },
      {
        path: 'demeanor',
        name: 'demeanor',
        meta: {
          title: '大神风采',
          icon: 'el-icon-warning-outline',
          hidden: true
        },
        component: () => import('../Backend/demeanor')
      },
      {
        path: 'aboutUs',
        name: 'aboutUs',
        meta: {
          title: '关于我们',
          icon: 'el-icon-warning-outline',
          hidden: true
        },
        component: () => import('../Backend/aboutUs')
      },

    ]
  },

  //轮播图管理
  {
    path: '/chart',
    component: Layout,
    meta: {
      title: '轮播图管理',
      icon: 'el-icon-coin',
      hidden: true,
    },
    children: [
      {
        path: 'image',
        name: 'image',
        meta: {
          title: '轮播图管理',
          icon: 'el-icon-coin',
        },
        component: () => import('../Backend/chart'),
      },
    ],
  }

]
export default routes