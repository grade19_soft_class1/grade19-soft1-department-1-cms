//引入需要的组件
import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';

//全局注册路由中间件
Vue.use(VueRouter)

//定义一个路由实例，并且提供了这个路由实例的模式和路由定义
let router = new VueRouter({
    mode: 'history',
    routes
});

//导航守卫
router.beforeEach((to, from, next) => {
    //如果用户要去登录界面或者注册界面
    if (to.path === '/login' || to.path === "/register") {
        //如果用户已经登录就跳转到首页
        let token = localStorage.getItem('token');
        if (token) {
            next('/');
        } else {
            //如果用户没有登录
            next();
        }
    } else if (to.path === '/') {
        next();
    } else {
        //如果用户去登录界面和注册界面之外的所有界面
        let token = localStorage.getItem('token');
        if (token) {
            next()
        } else {
            //如果用户没有登录
            next('/login');
        }
    }
});

//暴露路由实例
export default router