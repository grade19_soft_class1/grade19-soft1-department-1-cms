// 定义一些常量
const TokenKey='token'
const RefreshTokenKey='refreshToken'

// 获取token
export function getToken(){
  return localStorage.getItem(TokenKey) || ''
}
//获取username
export function getUsername(){
  return localStorage.getItem('username') || ''
}

//获取用户Id
export function getUserId(){
  return localStorage.getItem('Id');
}

// 获取refreshToken
export function getRefreshToken(){
  return localStorage.getItem(RefreshTokenKey) || ''
}

// 保存token和refreshToken 和 username Id
export function setToken(token,refreshToken,username,id){
  localStorage.setItem(TokenKey,token)
  localStorage.setItem(RefreshTokenKey,refreshToken)
  localStorage.setItem("username",username)
  localStorage.setItem("Id",id)
}

// 清除token和refreshToken 和 username
export function clearToken(){
  localStorage.removeItem(TokenKey)
  localStorage.removeItem(RefreshTokenKey)
  localStorage.removeItem('username')
  localStorage.removeItem('Id',)
}

// 判断是否为登录状态
export function isLogin(){
  let token=getToken()
  if(token){
    return true
  }else{
    return false
  }
}
