import axios from "axios";
import { getToken } from "./auth";

//跨域
const instance = axios.create({
    baseURL: 'http://localhost:5000/',
    timeout: 5000,
});


//拦截器
instance.interceptors.request.use(config => {
    let token = getToken();
    if (token) {
        config.headers['Authorization'] = 'Bearer ' + token;
    }
    console.log(config);
    return config;
},
    err => {
        console.log(err);
        return Promise.reject(err);
    });

instance.interceptors.response.use(
    (response) => {
        console.log(response);
        if (response.status === 1000) {
            return response.data
        }
        return response
    }, (err) => {
        console.log(err);
        return Promise.reject(err)
    }
)

export default instance