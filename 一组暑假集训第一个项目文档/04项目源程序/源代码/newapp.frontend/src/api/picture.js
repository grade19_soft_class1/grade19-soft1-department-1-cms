import request from '../Utils/request'

//表格分页的get
export function GetPicture(params){
    return request.get(`/Picture`,{params:params});
}
//轮播图的get
export function Get(){
    return request.get(`/Picture/get`);
}
export function getById(id){
    return request.get(`/picture/${id}`)
}
export function updatePut(id,data){
    return request.put(`/picture/${id}`,data)
}
export function deleteId(id){
    return request.delete(`/picture/${id}`);
}