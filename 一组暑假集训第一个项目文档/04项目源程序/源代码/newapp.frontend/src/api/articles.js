import request from '../Utils/request'


export function GetArticle(params) {
    return request.get(`/articles`,{params:params});
}

export function GetArticleById(id) {
    return request.get(`/articles/${id}`)
}
export function deleteId(id) {
    return request.delete(`/articles/${id}`)
}

export function addArticles(data) {
    return request.post(`/articles`, data);
}
//获取48小时点赞总数排序
export function GetReadTopten() {
    return request.get(`/articles/GetReadSum`);
}
//获取十天评论排行
export function GetCommentTopten() {
    return request.get(`/articles/GetCommentSum`);
}
//修改文章
export function updateArticles(id, data) {
    return request.put(`/articles/${id}`, data)
}
//点赞文章
export function PostArticleLike(data) {
    return request.post(`/articles/Like`, data);
}
//获取文章点赞
export function GetArticleLike(a, b) {
    return request.get(`/articles/getLike?userid=${a}&&articleId=${b}`);
}

//获取文章评论
export function GetArticleComment(a) {
    return request.get(`/Articles/GetArticleComment?articleId=${a}`);
}

//发表评论
export function PostArticleComment(data) {
    return request.post(`/Articles/comment`, data);
}


