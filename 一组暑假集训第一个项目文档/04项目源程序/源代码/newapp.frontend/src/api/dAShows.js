import request from '../Utils/request'

export function GetDAShows(){
    return request.get(`/DAShows`);
}
export function deleteDAShows(id){
    return request.delete(`/DAShows/${id}`);
}
export function addDAShows(data){
    return request.post(`/DAShows`,data)
}
export function updateDAShows(id,data){
    return request.put(`/DAShows/${id}`,data)
}