import request from '../Utils/request'


export function GetCompanyDynamics(){
    return request.get(`/CompanyDynamics`);
}
export function deleteCompanyDynamics(id){
    return request.delete(`/CompanyDynamics/${id}`);
}
export function addCompanyDynamics(data){
    return request.post(`/CompanyDynamics`,data)
}
export function updateCompanyDynamics(id,data){
    return request.put(`/CompanyDynamics/${id}`,data)
}