import request from '../Utils/request'

export function GetUsers(params){
    return request.get(`/users`,{params:params})
}
export function getById(id){
    return request.get(`/users/${id}`)
}
export function addPost(data){
    return request.post(`/users`,data)
}
export function PasswordPost(data){
    return request.post(`/users/password`,data)
}
export function updatePut(id,data){
    return request.put(`/users/${id}`,data)
}
export function updatePicturePut(data){
    return request.post(`/users/userpicture`,data)
}
export function deleteId(id){
    return request.delete(`/users/${id}`)
}
export function Login(data){
    return request.post(`/users/token`,data);
}
export function Register(data){
    return request.post(`/users/`,data)
}


